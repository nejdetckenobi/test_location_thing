from tornado.web import RequestHandler
from http import HTTPStatus
from app.helpers import CITIES, calculate_city_score, get_weather_data
import json


class LocationHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieve information about a city.
        :param args:
        :param kwargs:
        :return:
        """
        city_name = args[0]
        city = CITIES.get(city_name.lower())
        if not city:
            self.set_status(HTTPStatus.BAD_REQUEST)
            self.write('Bad city. Very bad.')
            return
        weather_data = get_weather_data(city.city.lower())
        current_weather_data = weather_data['consolidated_weather'][0]
        description = current_weather_data['weather_state_name']
        current_temperature = current_weather_data['the_temp']
        response = {'city_name': city_name.lower(),
                    'current_temperature': current_temperature,
                    'current_weather_description': description,
                    'population': city.population,
                    'bars': city.bars,
                    'city_score': calculate_city_score(city)}
        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
