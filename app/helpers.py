from collections import namedtuple
from urllib.request import urlopen
from urllib.parse import urlencode
from csv import reader
from dateutil.parser import parse as dateparse
from datetime import datetime, timedelta
import json


DATA_FILEPATH = './data/european_cities.csv'
METAWEATHER_WOEID_API_URL = 'https://www.metaweather.com/api/location/search'
METAWEATHER_WEATHER_API_URL = 'https://www.metaweather.com/api/location/{}'
WOE_IDS = {}
WEATHER_CACHE = {}


def initialize_data(data_filepath):
    """
    Loads the city data from given CSV

    :param data_filepath:
    :return tuple_contains_data_class_and_city_list:
    """
    data_file = open(data_filepath)
    DATA = reader(data_file)

    # Because of the advantage of hashing, I go with dictionary.
    cities = {}

    # First line is the main line.
    City = namedtuple('City', next(DATA), verbose=False, rename=False)

    # I load all the data because this is not a huge file (21KB)
    # If it was like between 1000 and 10000 lines,
    # I would prefer a SQLite instance
    # Greater than 10K lines? Go for Postgres.
    for record in DATA:
        record[0] = int(record[0])
        record[1] = record[1].lower()
        record[3] = int(record[3])
        record[4] = int(record[4])
        record[5] = int(record[5])
        record[6] = int(record[6])
        record[7] = float(record[7])
        record[8] = float(record[8])
        cities[record[1]] = City(*record)
    return City, cities


def ask_metaweather_for_woeid(city_name):
    city_name = city_name.lower()
    r = urlopen(
        METAWEATHER_WOEID_API_URL + '?' + urlencode({'query': city_name})
    )
    location_data = r.read()
    r.close()
    return json.loads(location_data.decode())[0]['woeid']


def ask_metaweather_for_weather(city_name):
    city_name = city_name.lower()
    woe_id = WOE_IDS[city_name]

    # I assumed API will be available anytime and I will be online anytime.
    # If not, there should be a try/except block.

    r = urlopen(METAWEATHER_WEATHER_API_URL.format(woe_id))
    raw_data = r.read()
    r.close()
    weather_data = json.loads(raw_data.decode())
    return weather_data


def get_weather_data(city_name):
    city_name = city_name.lower()
    today = datetime.today().date()

    if city_name not in WOE_IDS:
        WOE_IDS[city_name] = ask_metaweather_for_woeid(city_name)

    data = WEATHER_CACHE.get(city_name)
    if not data:
        WEATHER_CACHE[city_name] = ask_metaweather_for_weather(city_name)

    elif today - dateparse(data['consolidated_weather'][0]['created']).date():
        WEATHER_CACHE[city_name] = ask_metaweather_for_weather(city_name)

    current_weather_data = WEATHER_CACHE[city_name]

    return current_weather_data


def calculate_city_score(city_name):
    # I couldn't imagine a score calculation function for now because
    #  because it's very subjective I think.
    # But in this scenario, you can use getter of weather data.
    #  or use raw city data imported from CSV.
    # For now, everyone like visiting cities with long names, only.
    return len(city_name)


City, CITIES = initialize_data(DATA_FILEPATH)
