from app.helpers import calculate_city_score
from tornado.web import RequestHandler
from http import HTTPStatus
import json


class LocationComparisonHandler(RequestHandler):

    def data_received(self, chunk):
        pass

    def __init__(self, application, request, **kwargs):
        super().__init__(application, request, **kwargs)

    def initialize(self, **kwargs):
        super().initialize()

    def get(self, *args, **kwargs):
        """
        Retrieves information about multiple cities, rates them and returns a ranking and score for each city.
        :param args:
        :param kwargs:
        :return:
        """
        cities = list(set(args[0].strip(',').split(',')))
        response = {'city_data': []}

        data = sorted([(c, calculate_city_score(c)) for c in cities],
                      key=lambda x: x[1], reverse=True)

        for index, record in enumerate(data):
            response['city_data'].append({'city_name': record[0],
                                          'city_rank': index + 1,
                                          'city_score': record[1]})
        # response = {'city_data': [{'city_name': 'greatplace',
        #                            'city_rank': 1,
        #                            'city_score': 8.4},
        #                           {'city_name': 'okplace',
        #                            'city_rank': 2,
        #                            'city_score': 5.2},
        #                           {'city_name': 'badplace',
        #                            'city_rank': 3,
        #                            'city_score': 2.2}
        #                           ]}

        self.set_status(HTTPStatus.OK)
        self.write(json.dumps(response))
